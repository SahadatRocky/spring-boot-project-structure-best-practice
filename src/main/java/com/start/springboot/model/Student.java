package com.start.springboot.model;

import com.start.springboot.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Student")
@Table(name = "student")
@EqualsAndHashCode(callSuper = true)
public class Student extends BaseEntity {

//    @Id
//    @Column(name = "id")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "age")
    private Integer age;

   @OneToOne(cascade = CascadeType.ALL)
   @JoinColumn(
           name = "student_id_card_id",
           referencedColumnName = "id"
   )

   private StudentIdCard studentIdCard;


}
