package com.start.springboot.model;

import com.start.springboot.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "StudentIdCard")
@Table(name = "Student_Id_Card")
public class StudentIdCard extends BaseEntity {
//    @Id
//    @Column(name = "id")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;

    @Column(name = "card_number")
    private String cardNumber;
}
