package com.start.springboot.service;

import com.start.springboot.base.BaseRepository;
import com.start.springboot.base.BaseService;
import com.start.springboot.constant.AppConstant;
import com.start.springboot.dtos.student.*;
import com.start.springboot.exception.ServiceExceptionHolder;
import com.start.springboot.model.Student;
import com.start.springboot.model.StudentIdCard;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.stream.Collectors;

@Slf4j
@Service("studentService")
public class StudentService extends BaseService<Student, StudentBean> {

    private final StudentIdCardService studentIdCardService;

    public StudentService(BaseRepository<Student> repository, ModelMapper modelMapper,
                          StudentIdCardService studentIdCardService) {
        super(repository, modelMapper);
        this.studentIdCardService = studentIdCardService;
    }

    public Page<StudentListBean> getStudentList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("age")));
        Page<Student> page = getRepository().findAll(pageRequest);

        return new PageImpl<>(page.getContent().stream().map(this::getStudentListBean).collect(Collectors.toUnmodifiableList()), page.getPageable(), page.getTotalElements());
    }

    private StudentListBean getStudentListBean(Student student) {
//
        return StudentListBean.builder()
                .id(student.getId())
                .name(student.getName())
                .email(student.getEmail())
                .age(student.getAge())
                .status(student.getStatus() ? AppConstant.ACTIVE : AppConstant.INACTIVE)
                .cardNumber(student.getStudentIdCard().getCardNumber())
                .build();
    }

    public StudentDetailsBean getStudentById(String id) {

        Student student  = getById(id);
        StudentDetailsBean studentDetailsBean = getModelMapper().map(student, StudentDetailsBean.class);
        return studentDetailsBean;
    }

    public StudentBean createStudent(StudentCreateBean studentCreateBean) {
//
        Student student = getModelMapper().map(studentCreateBean, Student.class);
        student.getStudentIdCard().setDateCreated(Instant.now());
        student.getStudentIdCard().setDateModified(Instant.now());
        return create(student);
    }

    public StudentDetailsBean updateStudent(String id, StudentEditBean studentEditBean) {

        Student student = getById(id);
        student.setName(studentEditBean.getName());
        student.setEmail(studentEditBean.getEmail());
        student.setAge(studentEditBean.getAge());
        student.setDateCreated(Instant.now());
        student.setDateModified(Instant.now());
        student.getStudentIdCard().setCardNumber(studentEditBean.getStudentIdCard().getCardNumber());
        student.getStudentIdCard().setDateCreated(Instant.now());
        student.getStudentIdCard().setDateModified(Instant.now());
        getRepository().save(student);
       return getStudentById(student.getId());
    }

    public String deleteStudent(String id) {
        getRepository().deleteById(id);
        return "delete successful";
    }
}
