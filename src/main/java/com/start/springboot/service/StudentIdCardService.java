package com.start.springboot.service;

import com.start.springboot.base.BaseRepository;
import com.start.springboot.base.BaseService;

import com.start.springboot.constant.AppConstant;
import com.start.springboot.dtos.student.StudentDetailsBean;
import com.start.springboot.dtos.studentIdCard.*;
import com.start.springboot.exception.ServiceExceptionHolder;
import com.start.springboot.model.Student;
import com.start.springboot.model.StudentIdCard;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.stream.Collectors;

@Slf4j
@Service("studentIdCardService")
public class StudentIdCardService extends BaseService<StudentIdCard, StudentIdCardBean> {

    public StudentIdCardService(BaseRepository<StudentIdCard> repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    public Page<StudentIdCardListBean> getStudentIdCardList(Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(Sort.Order.desc("status")));
        Page<StudentIdCard> page = getRepository().findAll(pageRequest);

        return new PageImpl<>(page.getContent().stream().map(this::getStudentIdCardListBean).collect(Collectors.toUnmodifiableList()),page.getPageable(),page.getTotalElements());
    }

    private StudentIdCardListBean getStudentIdCardListBean(StudentIdCard studentIdCard) {

        StudentIdCardListBean studentIdCardListBean = getModelMapper().map(studentIdCard, StudentIdCardListBean.class);
        return studentIdCardListBean;
    }

    public StudentIdCardDetailsBean getgetStudentIdCardById(String id) {
        StudentIdCard studentIdCard = getById(id);
        StudentIdCardDetailsBean studentIdCardDetailsBean = getModelMapper().map(studentIdCard, StudentIdCardDetailsBean.class);
        return studentIdCardDetailsBean;
    }

    public StudentIdCardBean createStudentIdCard(StudentIdCardCreateBean studentIdCardCreateBean) {
        StudentIdCard studentIdCard = getModelMapper().map(studentIdCardCreateBean, StudentIdCard.class);
        return create(studentIdCard);
    }

    public Object updateStudentIdCard(String id, StudentIdCardEditBean studentIdCardEditBean) {
        StudentIdCard studentIdCard = getRepository().findById(id).orElseThrow(() -> new ServiceExceptionHolder.IdNotFoundInDBException("No student id card found by id:" + id));
        BeanUtils.copyProperties(studentIdCardEditBean, studentIdCard, AppConstant.IGNORE_PROPERTIES);
        return update(id, studentIdCard);
    }

    public String deleteStudentIdcard(String id) {
        getRepository().deleteById(id);
        return "delete successful";
    }
}
