package com.start.springboot.repository;

import com.start.springboot.base.BaseRepository;
import com.start.springboot.model.StudentIdCard;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentIdCardRepository extends BaseRepository<StudentIdCard> {
}
