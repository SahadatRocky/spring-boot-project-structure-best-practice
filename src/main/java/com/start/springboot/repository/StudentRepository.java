package com.start.springboot.repository;

import com.start.springboot.base.BaseRepository;
import com.start.springboot.model.Student;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends BaseRepository<Student> {

}
