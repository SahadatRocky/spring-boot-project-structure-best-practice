package com.start.springboot.controller;

import com.start.springboot.dtos.student.StudentBean;
import com.start.springboot.dtos.student.StudentCreateBean;
import com.start.springboot.dtos.student.StudentEditBean;
import com.start.springboot.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("students")
    public ResponseEntity<?> getList(Pageable pageable){
        return new ResponseEntity<>(studentService.getStudentList(pageable), HttpStatus.OK);
    }

    @GetMapping("student/{id}")
    public ResponseEntity<?> getStudentById(@PathVariable("id") String id){
        return new ResponseEntity<>(studentService.getStudentById(id), HttpStatus.OK);
    }
    @PostMapping("student")
    public ResponseEntity<?> createStudent(@RequestBody @Valid StudentCreateBean studentCreateBean){
         return new ResponseEntity<>(studentService.createStudent(studentCreateBean),HttpStatus.OK);
    }
    @PutMapping("student/{id}")
    public ResponseEntity<?> updateStudent(@PathVariable("id") String id, @RequestBody @Valid StudentEditBean studentEditBean){
        return new ResponseEntity<>(studentService.updateStudent(id,studentEditBean), HttpStatus.OK);
    }
    @DeleteMapping("student/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable("id") String id){
        return new ResponseEntity<>(studentService.deleteStudent(id), HttpStatus.OK);
    }
}
