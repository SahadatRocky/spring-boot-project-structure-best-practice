package com.start.springboot.controller;

import com.start.springboot.dtos.student.StudentCreateBean;
import com.start.springboot.dtos.student.StudentEditBean;
import com.start.springboot.dtos.studentIdCard.StudentIdCardCreateBean;
import com.start.springboot.dtos.studentIdCard.StudentIdCardEditBean;
import com.start.springboot.service.StudentIdCardService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AdminV1API
@AllArgsConstructor
public class StudentIdCardController {

    private final StudentIdCardService studentIdCardService;

    @GetMapping("cards")
    public ResponseEntity<?> getList(Pageable pageable){
        return new ResponseEntity<>(studentIdCardService.getStudentIdCardList(pageable), HttpStatus.OK);
    }

    @GetMapping("card/{id}")
    public ResponseEntity<?> getStudentIdCardById(@PathVariable("id") String id){
        return new ResponseEntity<>(studentIdCardService.getgetStudentIdCardById(id), HttpStatus.OK);
    }

    @PostMapping("card")
    public ResponseEntity<?> createStudentIdCard(@RequestBody @Valid StudentIdCardCreateBean studentIdCardCreateBean){
        return new ResponseEntity<>(studentIdCardService.createStudentIdCard(studentIdCardCreateBean),HttpStatus.OK);
    }

    @PutMapping("card/{id}")
    public ResponseEntity<?> updateStudentIdCard(@PathVariable("id") String id, @RequestBody @Valid StudentIdCardEditBean studentIdCardEditBean){
        return new ResponseEntity<>(studentIdCardService.updateStudentIdCard(id,studentIdCardEditBean), HttpStatus.OK);
    }

    @DeleteMapping("card/{id}")
    public ResponseEntity<?> deleteStudentIdCard(@PathVariable("id") String id){
        return new ResponseEntity<>(studentIdCardService.deleteStudentIdcard(id), HttpStatus.OK);
    }


}
