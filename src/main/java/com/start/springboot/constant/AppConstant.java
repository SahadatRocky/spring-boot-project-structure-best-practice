package com.start.springboot.constant;

public class AppConstant {

    public static final String TOKEN_TYPE = "Bearer";
    public static final String ACTIVE = "Active";
    public static final String INACTIVE = "Inactive";

    public static final String[] IGNORE_PROPERTIES = {"id", "dateCreated"};
}
