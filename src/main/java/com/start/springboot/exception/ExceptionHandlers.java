package com.start.springboot.exception;


import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlers {

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceExceptionHolder.ServerNotFoundException.class)
    public ResponseEntity<?> handleServerNotFoundException(final ServiceExceptionHolder.ServerNotFoundException ex) {
        return new ResponseEntity<>(new ExceptionBean(ex.getCode(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ServiceExceptionHolder.IdNotFoundInDBException.class)
    public ResponseEntity<?> handleIdNotFoundInDBException(final ServiceExceptionHolder.IdNotFoundInDBException ex) {
        return new ResponseEntity<>(new ExceptionBean(ex.getCode(), ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ServiceExceptionHolder.RefreshTokenException.class)
    public ResponseEntity<?> handleRefreshTokenException(final ServiceExceptionHolder.RefreshTokenException ex) {
        return new ResponseEntity<>(new ExceptionBean(ex.getCode(), ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceExceptionHolder.CustomException.class)
    public ResponseEntity<?> handleCustomException(final ServiceExceptionHolder.CustomException ex) {
        return new ResponseEntity<>(new ExceptionBean(ex.getCode(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error ->
                errors.put(error.getField(), error.getDefaultMessage()));
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({SQLIntegrityConstraintViolationException.class, EntityNotFoundException.class})
    public ResponseEntity<?> handleUniqueConstraintException(SQLIntegrityConstraintViolationException ex) {
        return new ResponseEntity<>(new ExceptionBean(ex.getErrorCode(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ExceptionHandler({BadCredentialsException.class})
//    public ResponseEntity<?> handleBadCredentialsException(BadCredentialsException ex) {
//        return new ResponseEntity<>(new ExceptionBean(1009, ex.getMessage()), HttpStatus.BAD_REQUEST);
//    }


    @Data
    @AllArgsConstructor
    private static class ExceptionBean {
        Integer code;
        String message;
    }
}
