package com.start.springboot.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

public class ServiceExceptionHolder {

    public final static int EXCEPTION_ID_NOT_FOUND_IN_DB = 1001;
    public final static int PASSWORD_MISMATCH_EXCEPTION_CODE = 1002;
    public final static int SPRING_SECURITY_EXCEPTION = 9000;
    public final static int REFRESH_TOKEN_EXCEPTION = 9001;

    @Getter
    @RequiredArgsConstructor
    public static class ServiceException extends RuntimeException {
        private final int code;
        private final String message;
    }

    public static class ResourceNotFoundException extends ServiceException {
        public ResourceNotFoundException(int code, String message) {
            super(code, message);
        }
    }

    public static class ServerNotFoundException extends ResourceNotFoundException {
        public ServerNotFoundException(final String msg, final int code) {
            super(code, msg);
        }
    }

    public static class IdNotFoundInDBException extends ResourceNotFoundException {
        public IdNotFoundInDBException(final String msg) {
            super(EXCEPTION_ID_NOT_FOUND_IN_DB, msg);
        }
    }

    public static class RefreshTokenException extends ResourceNotFoundException {
        public RefreshTokenException(final String msg) {
            super(REFRESH_TOKEN_EXCEPTION, msg);
        }
    }


    public static class CustomException extends ResourceNotFoundException {
        public CustomException(int code, String msg) {
            super(code, msg);
        }
    }
}
