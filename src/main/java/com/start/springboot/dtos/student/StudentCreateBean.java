package com.start.springboot.dtos.student;

import com.start.springboot.dtos.studentIdCard.StudentIdCardBean;
import com.start.springboot.model.StudentIdCard;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentCreateBean {
    private String name;
    private String email;
    private Integer age;
    private StudentIdCardBean studentIdCard;
}
