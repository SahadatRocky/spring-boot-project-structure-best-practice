package com.start.springboot.dtos.student;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentListBean {
    private String id;
    private String name;
    private String email;
    private Integer age;
    private String status;
    private String cardNumber;

}
