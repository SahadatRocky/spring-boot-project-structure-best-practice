package com.start.springboot.dtos.student;

import com.start.springboot.dtos.studentIdCard.StudentIdCardBean;
import com.start.springboot.dtos.studentIdCard.StudentIdCardEditBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentEditBean {
    private String id;
    private String name;
    private String email;
    private Integer age;
    private StudentIdCardEditBean studentIdCard;
}
