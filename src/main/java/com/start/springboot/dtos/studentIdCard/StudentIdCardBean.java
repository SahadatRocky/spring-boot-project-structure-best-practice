package com.start.springboot.dtos.studentIdCard;

import com.start.springboot.dtos.IRequestBodyDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentIdCardBean implements IRequestBodyDTO {
    private String id;
    private String cardNumber;
}
