package com.start.springboot.dtos.studentIdCard;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentIdCardCreateBean{
    private String cardNumber;
}
